package com.example.movieassesmentmandiri.base

import com.example.movieassesmentmandiri.common.Result
import retrofit2.Response
import timber.log.Timber

abstract class BaseDataSource {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Result<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Result.success(body)
            }
            return error("${response.code()} : ${response.message()}")
        } catch (e: Exception) {
            return error(e.message ?: e.toString())
        }
    }

    private fun <T> error(message: String): Result<T> {
        Timber.e(message)
        return Result.error("Network call has failed for a following reason : $message")
    }

    protected suspend fun <T> getResultPaging(call: suspend () -> T): Result<T> {
        return try {
            val response = call()
            if (response != null) {
                Result.success(response)
            }
            error("$response")
        } catch (e: Exception) {
            error(e.message ?: e.toString())
        }
    }

    private fun <T> errorPaging(message: String): Result<T> {
        Timber.e(message)
        return Result.error("Network call has failed for a following reason : $message")
    }
}