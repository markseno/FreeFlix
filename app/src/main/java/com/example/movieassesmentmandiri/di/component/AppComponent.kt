package com.example.movieassesmentmandiri.di.component

import com.example.movieassesmentmandiri.FreeFlixApp
import com.example.movieassesmentmandiri.di.module.ActivityBuilder
import com.example.movieassesmentmandiri.di.module.AppModule
import com.example.movieassesmentmandiri.di.module.NetworkModule
import com.example.movieassesmentmandiri.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ActivityBuilder::class,
        AppModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        AndroidSupportInjectionModule::class
    ]
)

interface AppComponent : AndroidInjector<FreeFlixApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: FreeFlixApp): Builder

        fun build(): AppComponent
    }

}