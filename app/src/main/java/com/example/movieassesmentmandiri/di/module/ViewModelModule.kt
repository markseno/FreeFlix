package com.example.movieassesmentmandiri.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.movieassesmentmandiri.di.ViewModelKey
import com.example.movieassesmentmandiri.di.factory.ViewModelFactory
import com.example.movieassesmentmandiri.ui.detail.DetailViewModel
import com.example.movieassesmentmandiri.ui.main.MainViewModel
import com.example.movieassesmentmandiri.ui.review.ReviewViewModel
import com.example.movieassesmentmandiri.ui.seeall.SeeAllViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun providesMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun providesDetailViewModel(viewModel: DetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReviewViewModel::class)
    internal abstract fun providesReviewViewModel(viewModel: ReviewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SeeAllViewModel::class)
    internal abstract fun providesSeeAllViewModel(viewModel: SeeAllViewModel): ViewModel
}