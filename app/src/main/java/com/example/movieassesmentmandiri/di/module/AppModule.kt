package com.example.movieassesmentmandiri.di.module

import android.app.Application
import android.content.Context
import com.example.movieassesmentmandiri.data.remote.MoviesService
import com.example.movieassesmentmandiri.FreeFlixApp
import com.example.movieassesmentmandiri.data.remote.MoviesRemoteDataSource
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: FreeFlixApp): Context = app

    @Provides
    @Singleton
    fun provideApplication(app: FreeFlixApp): Application = app

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): MoviesService =
        retrofit.create(MoviesService::class.java)

    @Provides
    @Singleton
    @Named("IO")
    fun provideBackgroundDispatchers(): CoroutineDispatcher =
        Dispatchers.IO

    @Provides
    @Singleton
    @Named("MAIN")
    fun provideMainDispatchers(): CoroutineDispatcher =
        Dispatchers.Main

    @Provides
    @Singleton
    fun provideRemoteDataSource(apiService: MoviesService) = MoviesRemoteDataSource(apiService)
}