package com.example.movieassesmentmandiri.di.module

import com.example.movieassesmentmandiri.ui.detail.DetailActivity
import com.example.movieassesmentmandiri.ui.main.MainActivity
import com.example.movieassesmentmandiri.ui.review.ReviewActivity
import com.example.movieassesmentmandiri.ui.seeall.SeeAllActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributesDetailActivity(): DetailActivity

    @ContributesAndroidInjector
    abstract fun contributesReviewActivity(): ReviewActivity

    @ContributesAndroidInjector
    abstract fun contributesSeeAllActivity(): SeeAllActivity

}