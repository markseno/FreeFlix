package com.example.movieassesmentmandiri.ui.seeall

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieassesmentmandiri.R
import com.example.movieassesmentmandiri.data.models.genre.Genre
import com.example.movieassesmentmandiri.databinding.ItemGenreBinding

class GenreAdapter(private  val onClick: (Genre) -> Unit, private val list: List<Genre>, private var genreChoosen: ArrayList<String>) :
    RecyclerView.Adapter<GenreAdapter.Holder>() {

    @SuppressLint("NotifyDataSetChanged")
    internal fun updateGenreSelected(genreChoosen: ArrayList<String>) {
        this.genreChoosen = arrayListOf()
        this.genreChoosen.addAll(genreChoosen)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemGenreBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }

    inner class Holder(private var binding: ItemGenreBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Genre?) = binding.apply {
            if (item == null) {
                binding.cardGenre.visibility = View.GONE
            } else {
                binding.tvGenre.text = item.name
                if (isGenreSelected(item.id.toString())) {
                    binding.cardGenre.setCardBackgroundColor(binding.root.context.getColor(R.color.greyLight))
                } else {
                    binding.cardGenre.setCardBackgroundColor(binding.root.context.getColor(R.color.white))
                }
                binding.cardGenre.setOnClickListener {
                    onClick(item)
                }
            }
        }
    }

    private fun isGenreSelected(genreId: String): Boolean {
        var count = 0
        genreChoosen.forEach {
            if (it == genreId) {
                count+=1
            }
        }
        return count>0
    }
}