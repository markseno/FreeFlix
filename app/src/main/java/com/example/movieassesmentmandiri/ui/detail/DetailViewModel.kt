package com.example.movieassesmentmandiri.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieassesmentmandiri.common.Result
import com.example.movieassesmentmandiri.data.MoviesRepository
import com.example.movieassesmentmandiri.data.models.detailmovie.DetailMovie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val repository: MoviesRepository
) : ViewModel() {

    private val _detailMovie = MutableLiveData<Result<DetailMovie>>()
    val detailMovie: LiveData<Result<DetailMovie>> = _detailMovie

    fun getDetailMovieById(movieId: Int) {
        viewModelScope.launch {
            repository.getDetailMovieById(movieId)
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    _detailMovie.value = result
                }
        }
    }

}