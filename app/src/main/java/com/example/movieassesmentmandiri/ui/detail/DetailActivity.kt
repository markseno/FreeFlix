package com.example.movieassesmentmandiri.ui.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieassesmentmandiri.R
import com.example.movieassesmentmandiri.base.BaseActivity
import com.example.movieassesmentmandiri.databinding.ActivityDetailBinding
import com.example.movieassesmentmandiri.ui.dialog.DialogErrorFragment
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.example.movieassesmentmandiri.common.Result
import com.example.movieassesmentmandiri.data.models.detailmovie.Cast
import com.example.movieassesmentmandiri.data.models.detailmovie.DetailMovie
import com.example.movieassesmentmandiri.data.models.detailmovie.TrailerMovie
import com.example.movieassesmentmandiri.data.models.genre.Genre
import com.example.movieassesmentmandiri.di.injectViewModel
import com.example.movieassesmentmandiri.ui.review.ReviewActivity
import com.example.movieassesmentmandiri.utils.Cons.ERROR_DIALOG
import com.example.movieassesmentmandiri.utils.Cons.LOADING_DIALOG
import com.example.movieassesmentmandiri.utils.convertStringIntoDateTime
import com.example.movieassesmentmandiri.utils.formatIntoK
import com.example.movieassesmentmandiri.utils.formatRate

class DetailActivity : BaseActivity<ActivityDetailBinding, DetailViewModel>(),
    DialogErrorFragment.DialogErrorFragmentListener {

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<DetailViewModel> = DetailViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_detail

    override fun getViewBinding(): ActivityDetailBinding { return ActivityDetailBinding.inflate(layoutInflater) }

    override fun initView() {
        showLoadingDialog()
        viewModel.getDetailMovieById(intent.getIntExtra("movieId", 0))
        observeDetailMovie()
        binding.scrollView.visibility = View.GONE
        binding.layoutHeader.tvTitle.text = this.getString(R.string.movie)
    }

    override fun initListener() {
        binding.layoutHeader.ivBack.setOnClickListener {
            finish()
        }

        binding.tvSeeAllReview.setOnClickListener {
            val intentReview = Intent(this, ReviewActivity::class.java)
            intentReview.putExtra("movieId", intent.getIntExtra("movieId", 0))
            startActivity(intentReview)
        }
    }

    private fun setupCastAdapter(list: List<Cast>) {
        val castMovieAdapter = CastMovieAdapter(list)

        binding.rvCast.layoutManager =
            LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL,
                false
            )

        binding.rvCast.adapter = castMovieAdapter
    }

    private fun observeDetailMovie() {
        viewModel.detailMovie.observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        prepareSpreadData(result.data)
                        setupCastAdapter(result.data.credits.cast)
                        binding.scrollView.visibility = View.VISIBLE
                        dismissDialog(LOADING_DIALOG)
                    } else {
                        showErrorDialog(result.message?: "Detail Movie Tidak Ditemukan")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun prepareSpreadData(detailMovie: DetailMovie) {
        binding.tvTitleMovie.text = detailMovie.title
        binding.tvMovieRate.text = formatRate(detailMovie.vote_average)
        binding.tvGenreMovie.text = prepareShowGenre(detailMovie.genres)
        binding.tvOverview.text = detailMovie.overview
        binding.tvTotalVotes.text = formatIntoK(detailMovie.vote_count) + " Votes"
        binding.tvReleaseYear.text = detailMovie.release_date.substring(0,4)

        binding.youtubePlayerVideo.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                youTubePlayer.loadVideo(getMovieTrailerId(detailMovie.videos.results), 0f)
            }
        })
    }

    private fun prepareShowGenre(
        list : List<Genre>
    ) : String {
        val listGenre : MutableList<String> = arrayListOf()
        list.forEach {
            listGenre.add(it.name)
        }

        return listGenre.toString().replace("[","").replace("]","")
    }

    private fun getMovieTrailerId(
        trailerMovies: List<TrailerMovie>
    ): String {
        var trailerMovieId = ""

        trailerMovies.sortedByDescending {
            convertStringIntoDateTime(it.published_at.substring(0,10))
        }

        trailerMovies.forEach {
            if (it.official){
                if (it.site.lowercase() == "youtube"){
                    if (it.type.lowercase() == "trailer"){
                        if (trailerMovieId.isEmpty()){
                            trailerMovieId = it.key
                        }
                    }
                }
            }
        }
        return trailerMovieId
    }

    override fun onDialogErrorFragmentListener() {
        dismissDialog(ERROR_DIALOG)
        showLoadingDialog()
        viewModel.getDetailMovieById(intent.getIntExtra("movieId", 0))
    }
}