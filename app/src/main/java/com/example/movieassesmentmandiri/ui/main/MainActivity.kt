package com.example.movieassesmentmandiri.ui.main

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.example.movieassesmentmandiri.R
import com.example.movieassesmentmandiri.base.BaseActivity
import com.example.movieassesmentmandiri.data.models.genre.Genre
import com.example.movieassesmentmandiri.databinding.ActivityMainBinding
import com.example.movieassesmentmandiri.di.injectViewModel
import www.sanju.zoomrecyclerlayout.ZoomRecyclerLayout
import com.example.movieassesmentmandiri.common.Result
import com.example.movieassesmentmandiri.data.models.movies.Movie
import com.example.movieassesmentmandiri.ui.detail.DetailActivity
import com.example.movieassesmentmandiri.ui.dialog.DialogErrorFragment
import com.example.movieassesmentmandiri.ui.seeall.SeeAllActivity
import com.example.movieassesmentmandiri.utils.Cons.ERROR_DIALOG
import com.example.movieassesmentmandiri.utils.Cons.LOADING_DIALOG

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(),
    DialogErrorFragment.DialogErrorFragmentListener {

    private var genreList: List<Genre> = arrayListOf()

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_main

    override fun getViewBinding(): ActivityMainBinding { return ActivityMainBinding.inflate(layoutInflater) }

    override fun initView() {
        binding.scrollView.visibility = View.GONE
        showLoadingDialog()
        observeData()
        viewModel.fetchAllData()
    }

    override fun initListener() {
        binding.tvSeeAllTopRated.setOnClickListener {
            val intent = Intent(this, SeeAllActivity::class.java)
            intent.putExtra("isTopRated", true)
            startActivity(intent)
        }

        binding.tvSeeAllMovie.setOnClickListener {
            val intent = Intent(this, SeeAllActivity::class.java)
            intent.putExtra("isTopRated", false)
            startActivity(intent)
        }
    }

    private fun setupAdapterUpComing(list: List<Movie>) {
        binding.rvUpcoming.apply {
            adapter = UpComingAdapter(list)
            layoutManager = ZoomRecyclerLayout(
                this@MainActivity, LinearLayoutManager.HORIZONTAL,
                false,
            )
            isNestedScrollingEnabled = false
            onFlingListener = null
        }

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.rvUpcoming)
    }

    private fun setupAdapterTopRated(list: List<Movie>, genreList: List<Genre>) {
        binding.rvTopRated.apply {
            adapter = TopRatedAdapter(list, genreList) { goToDetailMovie(it) }
            layoutManager = LinearLayoutManager(
                this@MainActivity, LinearLayoutManager.HORIZONTAL,
                false
            )
        }
    }

    private fun setupAdapterAllMovie(list: List<Movie>, genreList: List<Genre>) {
        binding.rvAllMovie.apply {
            adapter = AllMovieAdapter(list, genreList) { goToDetailMovie(it) }
            layoutManager = GridLayoutManager(this@MainActivity, 2)
        }
    }

    private fun observeData() {
        viewModel.genres.observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        genreList = arrayListOf()
                        genreList = result.data.genres
                    } else {
                        showErrorDialog("Data Genre Kosong")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }

        viewModel.upcomingMovies.observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        setupAdapterUpComing(result.data.results)
                    } else {
                        showErrorDialog("Data UpComing Kosong")
                    }
                }
                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }
                Result.Status.LOADING -> {
                }
            }
        }

        viewModel.topRatedMovies.observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        setupAdapterTopRated(result.data.results, genreList)
                    } else {
                        showErrorDialog("Data Popular Movie Kosong")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }

        viewModel.allMovies.observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        setupAdapterAllMovie(result.data.results, genreList)
                        dismissDialog(LOADING_DIALOG)
                        binding.scrollView.visibility = View.VISIBLE
                    } else {
                        showErrorDialog("Data Movie Kosong")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }
    }

    private fun goToDetailMovie(movieId: Int){
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("movieId",movieId)
        startActivity(intent)
    }

    override fun onDialogErrorFragmentListener() {
        dismissDialog(ERROR_DIALOG)
        showLoadingDialog()
        viewModel.fetchAllData()
    }
}