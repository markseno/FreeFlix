package com.example.movieassesmentmandiri.ui.review

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import javax.inject.Inject
import com.example.movieassesmentmandiri.data.MoviesRepository
import com.example.movieassesmentmandiri.data.models.review.Review
import kotlinx.coroutines.flow.Flow

class ReviewViewModel @Inject constructor(
    private val repository: MoviesRepository
) : ViewModel() {
    private var currentResult: Flow<PagingData<Review>>? = null

    fun fetchAllReviews(movieId: Int): Flow<PagingData<Review>> {
        val newResult: Flow<PagingData<Review>> =
            repository.getReviews(movieId).cachedIn(viewModelScope)
        currentResult = newResult
        return newResult
    }
}