package com.example.movieassesmentmandiri.ui.review

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieassesmentmandiri.R
import com.example.movieassesmentmandiri.data.models.review.Review
import com.example.movieassesmentmandiri.data.remote.MoviesService
import com.example.movieassesmentmandiri.databinding.ItemReviewBinding
import com.example.movieassesmentmandiri.utils.getRangeDate

class ReviewAdapter :
    PagingDataAdapter<Review, ReviewAdapter.ReviewViewHolder> (
        ReviewDiffCallback()
    ) {

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {

        return ReviewViewHolder(
            ItemReviewBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }

    inner class ReviewViewHolder(private var binding: ItemReviewBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Review?) = binding.apply {
            Glide.with(root.context)
                .load(MoviesService.BASE_URL_IMAGE.plus(item?.author_details?.avatar_path))
                .error(ContextCompat.getDrawable(itemView.context, R.drawable.rounded_grey))
                .circleCrop()
                .into(ivReviewer)

            tvReviewerName.text = item?.author_details?.name ?: "-"
            tvMovieRate.text = item?.author_details?.rating ?: "-"
            tvReviewDate.text = getRangeDate(item?.updated_at ?:"-")
            tvReview.text = item?.content?: "-"
        }
    }

    private class ReviewDiffCallback : DiffUtil.ItemCallback<Review>() {
        override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean {
            return oldItem == newItem
        }
    }
}