package com.example.movieassesmentmandiri.ui.seeall

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.movieassesmentmandiri.databinding.ItemNetworkStateBinding

class SeeAllLoadingStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<SeeAllLoadingStateAdapter.LoadStateViewHolder>() {

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {

        if (loadState is LoadState.Loading) {
            holder.binding.loading.isVisible = true
            holder.binding.tvErrorMessage.isVisible = false
            holder.binding.buttonRetry.isVisible = false

        } else {
            holder.binding.loading.isVisible = false
        }

        if (loadState is LoadState.Error) {
            holder.binding.tvErrorMessage.isVisible = true
            holder.binding.buttonRetry.isVisible = true
            holder.binding.tvErrorMessage.text = loadState.error.localizedMessage
        }

        holder.binding.buttonRetry.setOnClickListener {
            retry.invoke()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        return LoadStateViewHolder(
            ItemNetworkStateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    inner class LoadStateViewHolder(val binding: ItemNetworkStateBinding) :
        RecyclerView.ViewHolder(binding.root)
}