package com.example.movieassesmentmandiri.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieassesmentmandiri.data.MoviesRepository
import com.example.movieassesmentmandiri.data.models.movies.Movies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.example.movieassesmentmandiri.common.Result
import com.example.movieassesmentmandiri.data.models.genre.Genres

class MainViewModel @Inject constructor(
    private val repository: MoviesRepository
) : ViewModel() {

    private val _upcomingMovies = MutableLiveData<Result<Movies>>()
    val upcomingMovies: LiveData<Result<Movies>> = _upcomingMovies

    private val _topRatedMovies = MutableLiveData<Result<Movies>>()
    val topRatedMovies: LiveData<Result<Movies>> = _topRatedMovies

    private val _allMovies = MutableLiveData<Result<Movies>>()
    val allMovies: LiveData<Result<Movies>> = _allMovies

    private val _genres = MutableLiveData<Result<Genres>>()
    val genres: LiveData<Result<Genres>> = _genres

    fun fetchAllData() {
        fetchAllGenre()
    }

    private fun fetchUpcomingMovies() {
        viewModelScope.launch {
            repository.getUpcomingMovie()
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    _upcomingMovies .value = result
                    fetchTopRatedMovies()
                }
        }
    }

    private fun fetchTopRatedMovies() {
        viewModelScope.launch {
            repository.getTopRated()
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    _topRatedMovies .value = result
                    fetchAllMovies()
                }
        }
    }

    private fun fetchAllMovies() {
        viewModelScope.launch {
            repository.getAllMovie()
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    _allMovies .value = result
                }
        }
    }

    private fun fetchAllGenre() {
        viewModelScope.launch {
            repository.getGenreList()
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    _genres.value = result
                    fetchUpcomingMovies()
                }
        }
    }
}