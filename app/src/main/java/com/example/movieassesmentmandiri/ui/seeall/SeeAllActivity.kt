package com.example.movieassesmentmandiri.ui.seeall

import android.content.Intent
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import com.example.movieassesmentmandiri.R
import com.example.movieassesmentmandiri.base.BaseActivity
import com.example.movieassesmentmandiri.databinding.ActivitySeeAllBinding
import com.example.movieassesmentmandiri.di.injectViewModel
import com.example.movieassesmentmandiri.ui.dialog.DialogErrorFragment
import com.example.movieassesmentmandiri.common.Result
import com.example.movieassesmentmandiri.data.models.genre.Genre
import com.example.movieassesmentmandiri.ui.detail.DetailActivity
import com.example.movieassesmentmandiri.utils.Cons
import com.example.movieassesmentmandiri.utils.Cons.LOADING_DIALOG
import kotlinx.coroutines.flow.collectLatest

class SeeAllActivity : BaseActivity<ActivitySeeAllBinding, SeeAllViewModel>(),
    DialogErrorFragment.DialogErrorFragmentListener{

    private lateinit var seeAllAdapter : SeeAllAdapter
    private lateinit var genreAdapter : GenreAdapter
    private var getListMovie: Job? = null
    private var genreChosen: ArrayList<String> = arrayListOf()

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<SeeAllViewModel> = SeeAllViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_see_all

    override fun getViewBinding(): ActivitySeeAllBinding { return ActivitySeeAllBinding.inflate(layoutInflater) }

    override fun initView() {
        binding.layoutHeader.tvTitle.text = if (intent.getBooleanExtra("isTopRated", false)) "Top Rated" else "All Movies"
        viewModel.fetchAllGenre()
        observeGenreList()
    }

    override fun initListener() {
        binding.layoutHeader.ivBack.setOnClickListener {
            finish()
        }
    }

    private fun observeGenreList() {
        viewModel.allGenre.observe(this) { result ->
            when (result.status) {

                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        setupGenreAdapter(result.data.genres)
                        setupAdapter(result.data.genres)
                    } else {
                        showErrorDialog(result.message?: "Data Genre Kosong")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                    showLoadingDialog()
                }
            }
        }
    }

    private fun setupGenreAdapter(genreList: List<Genre>) {
        genreAdapter = GenreAdapter(this::onGenreClicked, genreList, genreChosen)

        binding.rvGenreList.layoutManager =
            LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL,
                false
            )

        binding.rvGenreList.adapter = genreAdapter
    }

    private fun setupAdapter(genreList: List<Genre>) {
        seeAllAdapter = SeeAllAdapter(
            { goToDetailMovie(it) },
            genreList)

        binding.rvSeeAll.apply {
            layoutManager = GridLayoutManager(this@SeeAllActivity, 2)
            setHasFixedSize(true)
            adapter = seeAllAdapter.withLoadStateFooter(
                footer = SeeAllLoadingStateAdapter { seeAllAdapter.retry() }
            )
        }

        seeAllAdapter.addLoadStateListener { loadState ->
            if (loadState.refresh is LoadState.Loading ||
                loadState.append is LoadState.Loading)

            else {
                dismissDialog(LOADING_DIALOG)
                val errorState = when {
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.prepend is LoadState.Error ->  loadState.prepend as LoadState.Error
                    loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                    else -> null
                }
                errorState?.let {
                    if (seeAllAdapter.itemCount == 0) showErrorDialog(it.error.message?:"Unknown Error")
                }
            }
        }

        startLoadMovieList("")
    }

    private fun startLoadMovieList(genreId: String) {
        getListMovie?.cancel()
        getListMovie = lifecycleScope.launch {
            viewModel.fetchAllMovies(genreId, intent.getBooleanExtra("isTopRated", false))
                .collectLatest {
                    seeAllAdapter.submitData(this@SeeAllActivity.lifecycle, it)
                }
        }
    }

    private fun goToDetailMovie(movieId: Int){
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("movieId",movieId)
        startActivity(intent)
    }

    override fun onDialogErrorFragmentListener() {
        dismissDialog(Cons.ERROR_DIALOG)
        showLoadingDialog()
        viewModel.fetchAllGenre()
    }

    private fun onGenreClicked(genre: Genre) {
        if (isGenreSelected(genre.id.toString())) {
            genreChosen.remove(genre.id.toString())
        } else {
            genreChosen.add(genre.id.toString())
        }
        genreAdapter.updateGenreSelected(genreChosen)
        startLoadMovieList(genreChosen.toString().replace("[", "").replace("]", ""))
    }

    private fun isGenreSelected(genreId: String): Boolean {
        var count = 0
        genreChosen.forEach {
            if (it == genreId) {
                count+=1
            }
        }
        return count>0
    }
}