package com.example.movieassesmentmandiri.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieassesmentmandiri.R
import com.example.movieassesmentmandiri.data.models.movies.Movie
import com.example.movieassesmentmandiri.data.remote.MoviesService
import com.example.movieassesmentmandiri.databinding.ItemUpcomingBinding

class UpComingAdapter(private val list: List<Movie>) : RecyclerView.Adapter<UpComingAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemUpcomingBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }

    override fun getItemCount(): Int = if (list.size >= 10) 10 else list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }

    inner class Holder(private var binding: ItemUpcomingBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Movie) = binding.apply {
            Glide.with(root.context)
                .load(MoviesService.BASE_URL_IMAGE.plus(item.poster_path))
                .error(ContextCompat.getDrawable(itemView.context, R.color.greyLight))
                .into(ivMoviePoster)
        }
    }

}