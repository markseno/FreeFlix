package com.example.movieassesmentmandiri.ui.review

import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieassesmentmandiri.R
import com.example.movieassesmentmandiri.base.BaseActivity
import com.example.movieassesmentmandiri.databinding.ActivityReviewBinding
import com.example.movieassesmentmandiri.di.injectViewModel
import com.example.movieassesmentmandiri.ui.dialog.DialogErrorFragment
import com.example.movieassesmentmandiri.utils.Cons
import com.example.movieassesmentmandiri.utils.Cons.LOADING_DIALOG
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ReviewActivity : BaseActivity<ActivityReviewBinding, ReviewViewModel>(),
    DialogErrorFragment.DialogErrorFragmentListener {

    private var getListReview: Job? = null

    private lateinit var reviewAdapter : ReviewAdapter

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<ReviewViewModel> = ReviewViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_review

    override fun getViewBinding(): ActivityReviewBinding { return ActivityReviewBinding.inflate(layoutInflater) }

    override fun initView() {
        showLoadingDialog()
        setupAdapter()
        binding.layoutHeader.tvTitle.text = this.getString(R.string.review)
    }

    override fun initListener() {
        binding.layoutHeader.ivBack.setOnClickListener {
            finish()
        }
    }

    private fun setupAdapter() {
        reviewAdapter = ReviewAdapter()

        binding.rvReview.apply {
            layoutManager =
                LinearLayoutManager(
                    this@ReviewActivity, LinearLayoutManager.VERTICAL,
                    false
                )
            adapter = reviewAdapter.withLoadStateFooter(
                footer = ReviewLoadingStateAdapter { reviewAdapter.retry() }
            )
            setHasFixedSize(true)
        }

        reviewAdapter.addLoadStateListener { loadState ->

            if (loadState.refresh is LoadState.Loading ||
                loadState.append is LoadState.Loading)

            else {
                dismissDialog(LOADING_DIALOG)
                val errorState = when {
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.prepend is LoadState.Error ->  loadState.prepend as LoadState.Error
                    loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                    else -> null
                }
                errorState?.let {
                    if (reviewAdapter.itemCount == 0) showErrorDialog(it.error.message?:"Unknown Error")
                }
            }
        }

        startLoadReview()
    }

    private fun startLoadReview() {
        getListReview?.cancel()
        getListReview = lifecycleScope.launch {
            viewModel.fetchAllReviews(intent.getIntExtra("movieId", 0))
                .collectLatest {
                    reviewAdapter.submitData(it)
                }
        }
    }

    override fun onDialogErrorFragmentListener() {
        dismissDialog(Cons.ERROR_DIALOG)
        showLoadingDialog()
        startLoadReview()
    }
}