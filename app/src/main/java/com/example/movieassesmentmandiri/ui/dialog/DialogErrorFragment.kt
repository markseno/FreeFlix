package com.example.movieassesmentmandiri.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.movieassesmentmandiri.base.BaseDialog
import com.example.movieassesmentmandiri.databinding.DialogErrorBinding
import com.example.movieassesmentmandiri.utils.Cons

class DialogErrorFragment : BaseDialog<DialogErrorBinding>() {

    private var message: String = ""

    private var listener: DialogErrorFragmentListener? = null

    fun newInstance(bundle: Bundle): DialogErrorFragment {
        val fragment = DialogErrorFragment()
        fragment.arguments = bundle
        return fragment
    }

    interface DialogErrorFragmentListener {
        fun onDialogErrorFragmentListener()
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DialogErrorBinding
        get() = DialogErrorBinding::inflate

    override fun getDataForInit() {
        super.getDataForInit()
        arguments?.let {
            message = it.getString(Cons.MESSAGE_DIALOG)?: ""
        }
    }

    override fun bindView() {
        isCancelable = false

        binding.tvErrorText.text = message
    }

    override fun assignListener() {
        binding.tvRetry.setOnClickListener {
            dismiss()

            if (listener != null) {
                listener?.onDialogErrorFragmentListener()
                return@setOnClickListener
            }

            val dialogConfirmationListener = activity as DialogErrorFragmentListener?
            dialogConfirmationListener?.onDialogErrorFragmentListener()
        }
    }
}


