package com.example.movieassesmentmandiri.ui.seeall

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.movieassesmentmandiri.common.Result
import javax.inject.Inject
import com.example.movieassesmentmandiri.data.MoviesRepository
import com.example.movieassesmentmandiri.data.models.genre.Genre
import com.example.movieassesmentmandiri.data.models.genre.Genres
import com.example.movieassesmentmandiri.data.models.movies.Movie
import com.example.movieassesmentmandiri.data.models.movies.Movies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class SeeAllViewModel @Inject constructor(
    private val repository: MoviesRepository,
) : ViewModel() {
    private var currentResult: Flow<PagingData<Movie>>? = null

    private val _allGenre = MutableLiveData<Result<Genres>>()
    val allGenre: LiveData<Result<Genres>> = _allGenre

    fun fetchAllGenre() {
        viewModelScope.launch {
            repository.getGenreList()
                .flowOn(Dispatchers.IO)
                .collect { result ->
                    _allGenre.value = result
                }
        }
    }

    fun fetchAllMovies(genreId : String, isTopRated: Boolean): Flow<PagingData<Movie>> {
        val newResult: Flow<PagingData<Movie>> =
            repository.getMovies(genreId, isTopRated).cachedIn(viewModelScope)
        currentResult = newResult
        return newResult
    }

}