package com.example.movieassesmentmandiri.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieassesmentmandiri.R
import com.example.movieassesmentmandiri.data.models.genre.Genre
import com.example.movieassesmentmandiri.data.models.movies.Movie
import com.example.movieassesmentmandiri.data.remote.MoviesService
import com.example.movieassesmentmandiri.databinding.ItemTopRatedBinding

class TopRatedAdapter(private val list: List<Movie>, private val genreList: List<Genre>, private val onClick: (Int) -> Unit) :
    RecyclerView.Adapter<TopRatedAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemTopRatedBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }

    override fun getItemCount(): Int = if (list.size >= 10) 10 else list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }

    inner class Holder(private var binding: ItemTopRatedBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Movie?) = binding.apply {
            Glide.with(root.context)
                .load(MoviesService.BASE_URL_IMAGE.plus(item?.backdrop_path))
                .error(ContextCompat.getDrawable(itemView.context, R.color.greyLight))
                .into(ivMoviePoster)

            tvMovieName.text = item?.title ?: "-"
            tvMovieRate.text = item?.vote_average ?: "-"
            tvMovieGenre.text = prepareShowGenre(item?.genre_ids ?: emptyList())

            cvMain.setOnClickListener {
                onClick(item!!.id)
            }
        }
    }

    private fun prepareShowGenre(list: List<Int>): String {
        val listGenre: MutableList<String> = arrayListOf()
        list.forEach { id ->
            genreList.forEach { genre ->
                if (id == genre.id) {
                    listGenre.add(genre.name)
                }
            }
        }

        return listGenre.toString().replace("[", "").replace("]", "")
    }
}