package com.example.movieassesmentmandiri.utils

object Cons {
    const val ERROR_DIALOG = "errorDialog"
    const val LOADING_DIALOG = "loadingDialog"
    const val MESSAGE_DIALOG = "messageDialog"
}