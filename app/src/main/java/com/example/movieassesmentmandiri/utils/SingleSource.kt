package com.example.movieassesmentmandiri.utils

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import com.example.movieassesmentmandiri.common.Result
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.onStart

fun <R> resultFlow(
    networkCall: suspend () -> Result<R>
): Flow<Result<R>> =
    flow {
        val responseStatus = networkCall.invoke()
        if (responseStatus.status == Result.Status.SUCCESS) {
            responseStatus.data?.let { emit(Result.success(it)) }
        } else if (responseStatus.status == Result.Status.ERROR) {
            if (responseStatus.message != null) {
                emit(Result.error(responseStatus.message))
            }
        }
    }.onStart {
        emit(Result.loading())
    }.catch {
        emit(Result.error(it.message.toString()))
    }.distinctUntilChanged()


