package com.example.movieassesmentmandiri.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

fun Activity?.showDialogExt(dialogFragment: DialogFragment, tag: String?, fragmentManager: FragmentManager?) {
    if (!dialogFragment.isAdded) {
        this?.let {
            if (!it.isFinishing && !it.isDestroyed) {
                try {
                    fragmentManager?.let { it1 -> dialogFragment.show(it1, tag) }
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                }
            }
        }
    }
}

fun dismissDialogExt(tag: String, fragmentManager: FragmentManager?) {
    fragmentManager?.fragments?.forEach { fragment ->
        if (fragment is DialogFragment && fragment.tag == tag) {
            fragment.dismiss()
        }
    }
}


fun isDialogFragmentShowing(tag: String?, fragmentManager: FragmentManager?): Boolean {
    fragmentManager?.fragments?.forEach { fragment ->
        if (fragment is DialogFragment && fragment.tag == tag) {
            return fragment.dialog?.isShowing == true
        }
    }
    return false
}

fun convertStringIntoDateTime(raw: String?): Date? {
    return try {
        val formatSource = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        formatSource.isLenient = false
        formatSource.parse(raw.toString())
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}


fun formatIntoK(value: Int): String {
    val formatter = DecimalFormat("#.#")
    return if (value >= 1000) {
        val kFormatted = value.toDouble() / 1000.0
        formatter.format(kFormatted) + "K"
    } else {
        formatter.format(value)
    }
}

fun formatRate(value: Double): String {
    val formatter = DecimalFormat("#.#")
    return formatter.format(value)
}

@SuppressLint("SimpleDateFormat")
fun getRangeDate(date: String): String {
    if (date == "-") return date
    val currentDate = Date()

    val sdf = SimpleDateFormat("yyyy-MM-dd")
    val valueDate = sdf.parse(date)

    val valueCalendar = Calendar.getInstance()
    val currentCalendar = Calendar.getInstance()

    if (valueDate != null) {
        valueCalendar.time = valueDate
    }
    currentCalendar.time = currentDate

    val diffMillis = currentCalendar.timeInMillis - valueCalendar.timeInMillis

    val diffDays = diffMillis / (1000 * 60 * 60 * 24)
    val diffMonths = (currentCalendar.get(Calendar.YEAR) - valueCalendar.get(Calendar.YEAR)) * 12 +
            currentCalendar.get(Calendar.MONTH) - valueCalendar.get(Calendar.MONTH)
    val diffYears = currentCalendar.get(Calendar.YEAR) - valueCalendar.get(Calendar.YEAR)

    return if (diffYears > 0) {
        "$diffYears year ago"
    } else if (diffMonths > 0) {
        "$diffMonths month ago"
    } else {
        "$diffDays days ago"
    }
}


