package com.example.movieassesmentmandiri.data.models.detailmovie

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Cast(
    val id: String,
    val known_for_department: String,
    val character: String,
    val name: String,
    val profile_path: String
) : Parcelable