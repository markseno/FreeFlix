package com.example.movieassesmentmandiri.data.models.review

import android.os.Parcelable
import com.example.movieassesmentmandiri.data.models.review.AuthorReview
import kotlinx.parcelize.Parcelize

@Parcelize
data class Review(
    val id: String,
    val author_details: AuthorReview,
    val content: String,
    val updated_at: String,
) : Parcelable