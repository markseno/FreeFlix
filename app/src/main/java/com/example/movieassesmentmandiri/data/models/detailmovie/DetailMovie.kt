package com.example.movieassesmentmandiri.data.models.detailmovie

import android.os.Parcelable
import com.example.movieassesmentmandiri.data.models.genre.Genre
import kotlinx.parcelize.Parcelize

@Parcelize
data class DetailMovie(
    val id: Int,
    val vote_average: Double,
    val title: String,
    val overview: String,
    val release_date: String,
    val vote_count: Int,
    val videos: Videos,
    val genres: List<Genre>,
    val credits: Casts
) : Parcelable