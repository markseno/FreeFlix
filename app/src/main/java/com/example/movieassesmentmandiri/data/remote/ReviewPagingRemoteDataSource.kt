package com.example.movieassesmentmandiri.data.remote

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.movieassesmentmandiri.BuildConfig.TMDB_API_KEY
import com.example.movieassesmentmandiri.data.models.review.Review
import com.example.movieassesmentmandiri.data.remote.MoviesService.Companion.STARTING_PAGE_INDEX
import retrofit2.HttpException
import java.io.IOException

class ReviewPagingRemoteDataSource(private val moviesService: MoviesService, private val movieId : Int) :
    PagingSource<Int, Review>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Review> {
        val page = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response = moviesService.getReviewMovieById(movieId, TMDB_API_KEY, page)
            val review = response.results
            LoadResult.Page(
                data = review,
                prevKey = if (page == STARTING_PAGE_INDEX) null else page.minus(1),
                nextKey = if (response.results.isEmpty()) null else page.plus(1)
            )

        } catch (exception: IOException) {
            val error = IOException("Please Check Internet Connection")
            LoadResult.Error(error)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }

    }

    override fun getRefreshKey(state: PagingState<Int, Review>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

}