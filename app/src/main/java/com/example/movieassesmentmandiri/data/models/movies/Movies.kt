package com.example.movieassesmentmandiri.data.models.movies

import android.os.Parcelable
import com.example.movieassesmentmandiri.data.models.movies.Movie
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movies(
    val results: List<Movie>,
    val page: Int,
    val total_pages: Int,
    val total_results: Int
) : Parcelable