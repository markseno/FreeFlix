package com.example.movieassesmentmandiri.data.remote

import com.example.movieassesmentmandiri.BuildConfig
import com.example.movieassesmentmandiri.base.BaseDataSource
import javax.inject.Inject

class MoviesRemoteDataSource @Inject constructor(private val service: MoviesService) :
    BaseDataSource() {

    suspend fun getGenreList() = getResult {
        service.getGenreList(BuildConfig.TMDB_API_KEY)
    }

    suspend fun getMoviesWithGenre(page: Int) = getResult {
        service.getMoviesWithGenre(
            BuildConfig.TMDB_API_KEY,
            page
        )
    }

    suspend fun getDetailMovieById(movieId: Int) = getResult {
        service.getDetailMovieById(
            movieId,
            BuildConfig.TMDB_API_KEY,
            "videos,credits"
        )
    }

    suspend fun getUpcomingMovie(page: Int) = getResult {
        service.getUpcomingMovie(
            BuildConfig.TMDB_API_KEY,
            page
        )
    }

    suspend fun getTopRated(page: Int) = getResult {
        service.getTopRated(
            BuildConfig.TMDB_API_KEY,
            page
        )
    }
}