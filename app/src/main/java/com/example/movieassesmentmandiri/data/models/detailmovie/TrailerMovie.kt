package com.example.movieassesmentmandiri.data.models.detailmovie

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TrailerMovie(
    val id: String,
    val key: String,
    val site: String,
    val type: String,
    val published_at: String,
    val official: Boolean
) : Parcelable