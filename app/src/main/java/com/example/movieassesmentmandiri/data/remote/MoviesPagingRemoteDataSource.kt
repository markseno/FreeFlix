package com.example.movieassesmentmandiri.data.remote

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.movieassesmentmandiri.BuildConfig.TMDB_API_KEY
import com.example.movieassesmentmandiri.data.models.movies.Movie
import com.example.movieassesmentmandiri.data.remote.MoviesService.Companion.STARTING_PAGE_INDEX
import retrofit2.HttpException
import java.io.IOException

class MoviesPagingRemoteDataSource(private val moviesService: MoviesService, private val genreId : String, private val isTopRated: Boolean) :
    PagingSource<Int, Movie>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        val page = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response = if (isTopRated) moviesService.getTopRatedPaging(TMDB_API_KEY, genreId, page) else moviesService.getMoviesWithGenrePaging(TMDB_API_KEY, genreId, page)
            val movies = response.results
            LoadResult.Page(
                data = movies,
                prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1,
                nextKey = if (page >= response.total_pages) null else page + 1
            )

        } catch (exception: IOException) {
            val error = IOException("Please Check Internet Connection")
            LoadResult.Error(error)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }

    }

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

}