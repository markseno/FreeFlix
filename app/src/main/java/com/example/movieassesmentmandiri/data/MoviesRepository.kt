package com.example.movieassesmentmandiri.data

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.example.movieassesmentmandiri.data.models.movies.Movie
import com.example.movieassesmentmandiri.data.models.review.Review
import com.example.movieassesmentmandiri.data.remote.MoviesPagingRemoteDataSource
import com.example.movieassesmentmandiri.data.remote.MoviesRemoteDataSource
import com.example.movieassesmentmandiri.data.remote.MoviesService
import com.example.movieassesmentmandiri.data.remote.ReviewPagingRemoteDataSource
import com.example.movieassesmentmandiri.utils.resultFlow
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesRepository @Inject constructor(
    private val remote: MoviesRemoteDataSource,
    private val moviesService: MoviesService,
) {

    fun getGenreList() = resultFlow(
        networkCall = { remote.getGenreList() }
    )

    fun getUpcomingMovie() = resultFlow(
        networkCall = { remote.getUpcomingMovie(1) }
    )

    fun getTopRated() = resultFlow(
        networkCall = { remote.getTopRated(1) }
    )

    fun getAllMovie() = resultFlow(
        networkCall = { remote.getMoviesWithGenre(1)}
    )

    fun getDetailMovieById(movieId: Int) = resultFlow(
        networkCall = { remote.getDetailMovieById(movieId) }
    )
    fun getMovies(
        genreId: String,
        isTopRated: Boolean
    ): Flow<PagingData<Movie>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = 20),
            pagingSourceFactory = {
                MoviesPagingRemoteDataSource(moviesService,genreId,isTopRated)
            }
        ).flow
    }

    fun getReviews(
        movieId: Int
    ): Flow<PagingData<Review>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = 20),
            pagingSourceFactory = {
                ReviewPagingRemoteDataSource(moviesService, movieId)
            }
        ).flow
    }
}