package com.example.movieassesmentmandiri.data.models.detailmovie

import android.os.Parcelable
import com.example.movieassesmentmandiri.data.models.detailmovie.TrailerMovie
import kotlinx.parcelize.Parcelize

@Parcelize
data class Videos(
    val results: List<TrailerMovie>
) : Parcelable