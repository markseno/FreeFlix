package com.example.movieassesmentmandiri.data.models.detailmovie

import android.os.Parcelable
import com.example.movieassesmentmandiri.data.models.detailmovie.Cast
import kotlinx.parcelize.Parcelize

@Parcelize
data class Casts(
    val cast: List<Cast>
) : Parcelable