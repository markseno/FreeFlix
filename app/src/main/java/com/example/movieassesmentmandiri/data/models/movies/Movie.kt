package com.example.movieassesmentmandiri.data.models.movies

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(
    val id: Int,
    val title: String,
    val vote_average: String,
    val backdrop_path: String,
    val poster_path: String,
    val genre_ids: List<Int>
) : Parcelable