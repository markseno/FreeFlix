package com.example.movieassesmentmandiri.data.models.review

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ResultReviews(
    val results: List<Review>,
    val page: Int,
    val total_pages: Int,
    val total_results: Int
) : Parcelable