package com.example.movieassesmentmandiri.data.models.genre

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Genres(
    val genres: List<Genre>,
) : Parcelable