package com.example.movieassesmentmandiri.data.remote

import com.example.movieassesmentmandiri.data.models.detailmovie.DetailMovie
import com.example.movieassesmentmandiri.data.models.genre.Genres
import com.example.movieassesmentmandiri.data.models.movies.Movies
import com.example.movieassesmentmandiri.data.models.review.ResultReviews
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesService {

    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w300/"
        const val STARTING_PAGE_INDEX = 1
    }

    @GET("genre/movie/list")
    suspend fun getGenreList(
        @Query("api_key") api_key: String?,
    ):  Response<Genres>

    @GET("discover/movie")
    suspend fun getMoviesWithGenre(
        @Query("api_key") api_key: String?,
        @Query("page") page: Int?
    ): Response<Movies>

    @GET("discover/movie")
    suspend fun getMoviesWithGenrePaging(
        @Query("api_key") api_key: String?,
        @Query("with_genres") with_genres: String?,
        @Query("page") page: Int?
    ): Movies

    @GET("movie/{movie_id}")
    suspend fun getDetailMovieById(
        @Path(value = "movie_id", encoded = true) movieId: Int,
        @Query("api_key") api_key: String?,
        @Query("append_to_response") append_to_response: String?
    ):  Response<DetailMovie>

    @GET("movie/{movie_id}/reviews")
    suspend fun getReviewMovieById(
        @Path(value = "movie_id", encoded = true) movieId: Int,
        @Query("api_key") api_key: String?,
        @Query("page") page: Int?
    ): ResultReviews

    @GET("movie/upcoming")
    suspend fun getUpcomingMovie(
        @Query("api_key") api_key: String?,
        @Query("page") page: Int?
    ):  Response<Movies>

    @GET("movie/top_rated")
    suspend fun getTopRated(
        @Query("api_key") api_key: String?,
        @Query("page") page: Int?
    ):  Response<Movies>

    @GET("movie/top_rated")
    suspend fun getTopRatedPaging(
        @Query("api_key") api_key: String?,
        @Query("with_genres") with_genres: String?,
        @Query("page") page: Int?
    ): Movies
}