package com.example.movieassesmentmandiri.data.models.review

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AuthorReview(
    val name: String,
    val rating: String,
    val avatar_path: String,
) : Parcelable